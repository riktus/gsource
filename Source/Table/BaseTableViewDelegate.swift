//
//  BaseTableViewDelegate.swift
//  GSource
//
//  Created by Александр  Волков on 04.07.2018.
//

import Foundation
import UIKit

open class BaseTableViewDelegate : NSObject, UITableViewDelegate {
    
    private var mHeightForRow: CGFloat!
    
    private var mSelectionChangedHandler: ((Int) -> ())?
    
    open func onSelectionChanged(_ handler: @escaping (Int) -> () ) {
        mSelectionChangedHandler = handler
    }
    
    
    public init(heightForRow: CGFloat = UITableView.automaticDimension) {
        mHeightForRow = heightForRow
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if self.mSelectionChangedHandler != nil {
                self.mSelectionChangedHandler!(indexPath.row)
            }
        }
    }
    
    open func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return mHeightForRow
    }
}
