//
//  BaseTableVIewDataSource.swift
//  GSource
//
//  Created by Александр  Волков on 04.07.2018.
//

import Foundation
import UIKit

open class BaseTableViewDataSource<TSource: Any, TCell: UITableViewCell> : NSObject, UITableViewDataSource {
    
    private var mCellIdentifier: String!
    
    open var items: [TSource]!
    
    public init(cellIdentifier: String, data: [TSource]) {
        mCellIdentifier = cellIdentifier
        items = data
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: mCellIdentifier, for: indexPath) as? ICell {
            let item = items[indexPath.row]
            
            cell.configureCell(source: item)
            
            return cell as! TCell
        }
        
        return UITableViewCell()
    }
    
    open func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return nil
    }
    
    open func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    open func tableView(_ tableView: UITableView, commit editingStyle:   UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    open func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
        return "Удалить"
    }
    
    open func updateData(data: [TSource]) {
        items = data
    }
    
}
