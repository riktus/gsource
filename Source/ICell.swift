//
//  ICell.swift
//  GSource
//
//  Created by Александр  Волков on 04.07.2018.
//

import Foundation

public protocol ICell {
    func configureCell(source: Any)
}
