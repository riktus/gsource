//
//  BaseCollectionViewDataSource.swift
//  GSource
//
//  Created by Александр  Волков on 04.07.2018.
//

import Foundation
import UIKit

open class BaseCollectionViewDataSource<TSource: Any, TCell: UICollectionViewCell> : NSObject, UICollectionViewDataSource {
    private var mCellIdentifier: String!
    
    open var items: [TSource]!
    
    public init(cellIdentifier: String, data: [TSource]) {
        mCellIdentifier = cellIdentifier
        items = data
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mCellIdentifier, for: indexPath) as? ICell {
            let item = items[indexPath.row]
            cell.configureCell(source: item)
            
            return cell as! TCell
        }
        
        return UICollectionViewCell()
    }
    
    open func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    open func updateData(data: [TSource]) {
        items = data
    }
}
