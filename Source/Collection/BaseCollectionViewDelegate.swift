//
//  BaseCollectionViewDelegate.swift
//  GSource
//
//  Created by Александр  Волков on 04.07.2018.
//

import Foundation
import UIKit

open class BaseCollectionViewDelegate : NSObject, UICollectionViewDelegate {
    
    private var mSelectionChangedHandler: ((Int) -> ())?
    
    open func onSelectionChanged(_ handler: @escaping (Int) -> () ) {
        mSelectionChangedHandler = handler
    }
    
    open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if self.mSelectionChangedHandler != nil {
                self.mSelectionChangedHandler!(indexPath.row)
            }
        }
    }
    
}
